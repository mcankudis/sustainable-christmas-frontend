import { ABOUT_VIEW_SETTINGS } from './views/AboutView';
import { HOME_VIEW_SETTINGS } from './views/HomeView';
import { ViewSettings } from './types';

export const NavBarItems: ViewSettings[] = [HOME_VIEW_SETTINGS, ABOUT_VIEW_SETTINGS];
