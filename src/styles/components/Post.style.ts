import styled from 'styled-components';

export const StyledPost = styled.div`
    width: clamp(256px, 80%, 512px);
    margin: auto;
`;
