import styled from 'styled-components';

export const LoaderView = styled.div`
    height: calc(100% - 40px);
    display: flex;
    justify-content: center;
    align-items: center;
`;
