import styled from 'styled-components';

export const StyledError = styled.div`
    text-align: center;
    .header {
        font-size: 36px;
        font-weight: bold;
    }
`;
