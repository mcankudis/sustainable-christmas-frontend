import { FC } from 'react';
import { useAboutContext } from '../context/AboutContext';

export const AboutText: FC = () => {
    const state = useAboutContext();

    switch (state.type) {
        case 'LOADING':
            return <div></div>;
        case 'READY':
            return (
                <div className="ck-content" dangerouslySetInnerHTML={{ __html: state.about }}></div>
            );
        case 'ERROR':
            return <div>TODO: ERROR VIEW: {state.message}</div>;
        default:
            return <div>TODO: ERROR VIEW</div>;
    }
};
